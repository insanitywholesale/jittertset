def rain_amount(mm):
    mm = int(mm)
    if (mm < 40):
        aga = str(40 - mm)
        return "You need to give your plant " + aga + "mm of water"
    else:
        return "Your plant has had more than enough water for today!"
