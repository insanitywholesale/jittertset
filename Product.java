package testing;

public class Product {

	private int KwdikosProion;
	private int TimhXwris;
	private int KathgoriaFPA;
	
	Product(){}
	
	Product(int kp, int tx, int kfpa){
		
		KwdikosProion = kp;
		TimhXwris = tx;
		KathgoriaFPA = kfpa;
		
	}
	
	public String toString(){
	
		String s = "o kwdikos einai " + KwdikosProion + "    h timh xwris einai " + TimhXwris + "    h kathgoria FPA einai " + KathgoriaFPA;
		return s;
	}	
	
	int getKP() {return KwdikosProion;}
	int getTX() {return TimhXwris;}
	int getKFPA() {return KathgoriaFPA;}
	
	void setKP(int kp) {KwdikosProion = kp;}
	void setTX(int tx) {TimhXwris = tx;}
	void setKFPA(int kfpa) {KathgoriaFPA = kfpa;}
	
	
	public double getTelikhTimh(int kfpa, int tx, double timh) {
		
		
		//could be done with switch
		if (kfpa == 1)
			timh = tx + tx * 0.24;
		else if (kfpa == 2)
			timh = tx + tx * 0.13;
		else
			timh = tx + tx * 0.05;
		
		return timh;
	}
	
	
}
