#!/bin/bash


#make directory to put copies of the files in

mkdir -p /copies
chmod 644 /copies

#copy important files

cp /etc/default/grub /copies/
cp /etc/modprobe.d/vfio.conf /copies/
cp /etc/mkinitcpio.conf /copies/
cp /etc/libvirt/qemu.conf /copies/
